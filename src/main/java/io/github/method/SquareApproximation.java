package io.github.method;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Function;

public class SquareApproximation {

    public final int SCALE = 32;

    public BigDecimal findMinimum(Function<BigDecimal, BigDecimal> f, BigDecimal a, BigDecimal b, BigDecimal eps1, BigDecimal eps2) {

        final BigDecimal two = BigDecimal.ONE.add(BigDecimal.ONE).setScale(SCALE, RoundingMode.HALF_UP);
        final BigDecimal three = two.add(BigDecimal.ONE).setScale(SCALE, RoundingMode.HALF_UP);

        BigDecimal x1 = a.add(b).divide(two, RoundingMode.HALF_UP).setScale(SCALE, RoundingMode.HALF_UP);

        BigDecimal dx = b.subtract(x1).divide(three, RoundingMode.HALF_UP).setScale(SCALE, RoundingMode.HALF_UP);
        BigDecimal x2;
        BigDecimal fx1;
        BigDecimal fx2;
        BigDecimal x3;
        BigDecimal fx3;
        BigDecimal Fmin;
        BigDecimal xMin;
        BigDecimal xApr;

        BigDecimal result;

        output:
        do {
            x2 = x1.add(dx);

            fx1 = f.apply(x1);
            fx2 = f.apply(x2);


            if (fx1.compareTo(fx2) > 0) {
                x3 = x1.add(dx.multiply(two)).setScale(SCALE, RoundingMode.HALF_UP);
            } else {
                x3 = x1.subtract(dx);
            }

            fx3 = f.apply(x3);

            do {
                Fmin = fx1.min(fx2).min(fx3);
                if (Fmin.equals(fx1)) xMin = x1;
                else if (Fmin.equals(fx2)) xMin = x2;
                else xMin = x3;

                try {
                    xApr = x2.multiply(x2).subtract(x3.multiply(x3)).multiply(fx1)
                            .add(x3.multiply(x3).subtract(x1.multiply(x1)).multiply(fx2))
                            .add(x1.multiply(x1).subtract(x2.multiply(x2)).multiply(fx3))
                            .divide(
                                    x2.subtract(x3).multiply(fx1)
                                            .add(x3.subtract(x1).multiply(fx2))
                                            .add(x1.subtract(x2).multiply(fx3)), RoundingMode.HALF_UP
                            ).divide(two, RoundingMode.HALF_UP).setScale(SCALE, RoundingMode.HALF_UP);
                } catch (ArithmeticException e) {
                    x1 = xMin;
                    break;
                }

                BigDecimal fxApr = f.apply(xApr);

                boolean test1 = Fmin.subtract(fxApr).divide(fxApr, RoundingMode.HALF_UP).abs().compareTo(eps1) < 0;
                boolean test2 = xMin.subtract(xApr).divide(xApr, RoundingMode.HALF_UP).abs().compareTo(eps2) < 0;

                if (test1 && test2) {
                    result = xApr;
                    break output;
                } else if (xApr.compareTo(x1) >= 0 && xApr.compareTo(x3) <= 0) {
                    BigDecimal nextXMin = xMin.min(xApr);
                    if (nextXMin.compareTo(x2) > 0) {
                        x1 = x2;
                    } else {
                        x3 = x2;
                    }
                    x2 = nextXMin;
                } else {
                    x1 = xApr;
                    break;
                }
            } while (true);
        } while (true);

        return result.setScale(SCALE, RoundingMode.HALF_UP);
    }
}
