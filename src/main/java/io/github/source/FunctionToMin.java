package io.github.source;

import io.github.util.BigDecimalMath;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Function;

public class FunctionToMin implements Function<BigDecimal, BigDecimal> {

    @Override
    public BigDecimal apply(BigDecimal x) {
        BigDecimal three = BigDecimal.ONE
                .add(BigDecimal.ONE)
                .add(BigDecimal.ONE)
                .setScale(x.scale(), RoundingMode.HALF_UP);

        return x.multiply(x)
                .subtract(three.multiply(x))
                .add(x.multiply(BigDecimalMath.ln(x, x.scale())))
                .setScale(x.scale(), RoundingMode.HALF_UP);
    }

}
