package io.github;

import io.github.method.SquareApproximation;
import io.github.source.FunctionToMin;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Main {
    public static void main(String[] args) {
        System.out.println("minimum: " + new SquareApproximation().findMinimum(new FunctionToMin(),
                BigDecimal.ONE.setScale(32, RoundingMode.HALF_UP),
                BigDecimal.ONE.add(BigDecimal.ONE).setScale(32, RoundingMode.HALF_UP),
                new BigDecimal("0.0001"),
                new BigDecimal("0.0001")
                )
        );
    }
}